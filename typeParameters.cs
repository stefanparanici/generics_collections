﻿using System;

namespace generics_Collections
{
    class TypeParameters<TPar1,TPar2>
         where TPar1: class,new()
         where TPar2:IGeneric
    {
        public void ShowType()
        {
            Console.WriteLine($"par1 is {typeof(TPar1)}");
            Console.WriteLine($"par2 is {typeof(TPar2)}");
        }
    }
}

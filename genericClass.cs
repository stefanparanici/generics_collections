﻿using System;

namespace generics_Collections
{
    class GenericClass:IGeneric
    {
        public GenericClass(int num)
        {
            Num = num;
            Type();
        }
        public void Type()
        {
            Console.WriteLine("This class, implemetns IGeneric, and it has a constructor with parameters");
        }

        public int Num { get; set; }     
    }
}

﻿using System;

namespace generics_Collections
{
    class Program
    {
        static void Main()
        {
            TypeParameters<NewClass, GenericClass> typeParametersObject = new TypeParameters<NewClass, GenericClass>();
            Console.WriteLine("1.String in reverse:");
            Console.WriteLine(Reverse("Hello World!"));
            Console.WriteLine();
            Console.WriteLine("2.Generic Class, parameters types:");
            typeParametersObject.ShowType();
            Console.WriteLine();
            Console.WriteLine("3.List without doubles:");
            NoDoublesList<int> listaNoDoublesList = new NoDoublesList<int>();
            listaNoDoublesList.AddN(5);
            listaNoDoublesList.AddN(20);
            listaNoDoublesList.AddN(35);
            //listaNoDoublesList.AddN(35);
            // Daca se introduce o valoare, ce a fost introdusa in lista, se va returna o exceptie :)
            foreach (var elemnt in listaNoDoublesList)
            {
                Console.WriteLine(elemnt.ToString());
            }
        }

        static string Reverse<T1>(T1 rand)
            where T1 : class
        {
            char[] charArray = rand.ToString()?.ToCharArray();
            Array.Reverse(charArray ?? throw new InvalidOperationException());
            return new string(charArray);
        }
    }

    
}

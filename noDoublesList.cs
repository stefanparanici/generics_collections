﻿using System;
using System.Collections.Generic;

namespace generics_Collections
{
    class NoDoublesList<T>:List<T>
    {
   
        public void AddN(T element)
        {
            foreach (var listElement in this)
            {
                if (Equals(listElement, element))
                {
                    throw new Exception("Elementul se afla in lista !");
                }
            }
            this.Add(element);
        }
    }
}

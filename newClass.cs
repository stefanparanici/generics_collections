﻿using System;

namespace generics_Collections
{
    class NewClass
    {
        public NewClass()
        {
            Type();
        }
        public void Type()
        {
            Console.WriteLine("This class, does not implement IGeneric, and it has a constructor without parameters");
        }
    }
}
